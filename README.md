# **Modelli Probabilistici Per Le Decisioni**

## Progetto: Bayes Network on TripAdvisor Reviews

* Luca Gandolfi 807485
* Bruno Palazzi 806908
* Stefano Sacco 807532

### Introduction
The aim of the project is to create a bayesian model about prediction on TripAdvisor hotels' reviews.
In the repository, you can find Training data, Testing data, The vectorized representation of both folders and the code. 
The file bayesian_net.py is the core of the project.
In the Slides folder you can find a dash code about our presentation of this project.

### Libraries
The code is written in Python 2.7 and in Python 3, because a library was developed only for Python2. 
The must-have libraries for this project are:

* Pandas: for manipulating dataframes and csv
* libpgm: for create and run our bayesian model - only works on Python2
* sklearn: for performance measures
* dash: for the demo-presentation of this project

### Execution order
The correct order for a first execution is:

* feature_selection.py [optional, if you want to change the method for selecting terms]
* extract-extended.py
* remove_notvalid.py, but you have to modify the input csv to vettori.csv
* extract-test.py
* remove_notvalid.py, using the code as-is
* bayesian_net.py