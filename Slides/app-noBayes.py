#!/usr/bin/env python3
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd
import os
import plotly.plotly as py
import plotly.presentation_objs as pres
import base64
from collections import Counter

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

server = app.server

tabs_styles = {
    'height': '44px'
}
tab_style = {
    'borderBottom': '1px solid #d6d6d6',
    'padding': '6px',
    'fontWeight': 'bold'
}

tab_selected_style = {
    'borderTop': '1px solid #d6d6d6',
    'borderBottom': '1px solid #d6d6d6',
    'backgroundColor': '#119DFF',
    'color': 'white',
    'padding': '6px'
}

colors = {
    'background': '#111111',
    'text': '#7FDBFF'
}

#import dataset
df = pd.read_csv('../vettori-valid.csv')
OverallDict = df['Overall'].value_counts().to_dict()
RoomsDict = df['Rooms'].value_counts().to_dict()
ValueDict = df['Value'].value_counts().to_dict()
LocationDict = df['Location'].value_counts().to_dict()
CleanlinessDict = df['Cleanliness'].value_counts().to_dict()
ServiceDict = df['Service'].value_counts().to_dict()

def generate_table(dataframe, max_rows=10):
    return html.Table(
        # Header
        [html.Tr([html.Th(col) for col in dataframe.columns])] +

        # Body
        [html.Tr([
            html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
        ]) for i in range(min(len(dataframe), max_rows))]
    )

#import images
image_filename1 = '../Img/sample-review.png'
encoded_image1 = base64.b64encode(open(image_filename1, 'rb').read())

image_filename2 = '../Img/bayes-net.png'
encoded_image2 = base64.b64encode(open(image_filename2, 'rb').read())

image_filename3 = '../Img/bayes-net2.png'
encoded_image3 = base64.b64encode(open(image_filename3, 'rb').read())

image_filename4 = '../Img/slide1.png'
encoded_image4 = base64.b64encode(open(image_filename4, 'rb').read())

image_filename5 = '../Img/slide2.png'
encoded_image5 = base64.b64encode(open(image_filename5, 'rb').read())

image_filename6 = '../Img/bayes-net.png'
encoded_image6 = base64.b64encode(open(image_filename6, 'rb').read())

image_filename7 = '../Img/bayes-net2.png'
encoded_image7 = base64.b64encode(open(image_filename7, 'rb').read())


#load tabs
app.layout = html.Div([
    dcc.Tabs(id='tabs', style=tabs_styles, children=[
        #First tab
        dcc.Tab(label='Presentazione',
                style=tab_style,
                selected_style=tab_selected_style,
                children=[
                    html.Div([
                        html.Div([
                            html.Img(src='data:image/png;base64,{}'.format(encoded_image4.decode()),
                                    style={
                                        'height' : '88%',
                                        'width' : '100%',
                                        'float' : 'left',
                                        'position' : 'relative',
                                        'padding-top' : 0,
                                        'padding-right' : 0
                            }),
                        ]),
                    ])
		]),
		#Second tab
		dcc.Tab(label='Dati',
                style=tab_style,
                selected_style=tab_selected_style,
                children=[
                        dcc.Tabs(id="subtabs", children=[
                                    dcc.Tab(label='Formato iniziale dei dati', children=[
                                        html.Div([
                                            html.H1('Formato iniziale dei dati'),
                                            html.Div([
                        							html.Img(src='data:image/png;base64,{}'.format(encoded_image5.decode())),
                        					], style={'width':'49%', 'display':'inline-block', 'marginTop':'0%', 'color':'black'}),
                                            html.Div([
                                                html.Img(src='data:image/png;base64,{}'.format(encoded_image1.decode())),
                                            ],  style={'width':'49%', 'display':'inline-block', 'marginTop':'5%', 'color':'black'})
                                        ], style={'marginTop': '3%', 'textAlign': 'center'})
                    				]),
                                    dcc.Tab(label='Distribuzione Overall', children=[
                                        html.Div([
                                            dcc.Graph(
                                                id='example-graph',
                                                figure={
                                                    'data': [
                                                        {'x': [1], 'y': [OverallDict[1]],
                                                            'type': 'bar', 'name': 'Overall: 1'},
                                                        {'x': [2], 'y': [OverallDict[2]],
                                                            'type': 'bar', 'name': 'Overall: 2'},
                                                        {'x': [3], 'y': [OverallDict[3]],
                                                            'type': 'bar', 'name': 'Overall: 3'},
                                                        {'x': [4], 'y': [OverallDict[4]],
                                                            'type': 'bar', 'name': 'Overall: 4'},
                                                        {'x': [5], 'y': [OverallDict[5]],
                                                            'type': 'bar', 'name': 'Overall: 5'},
                                                    ]
                                                }
                                            )
                                        ])
                                    ]),
                                    dcc.Tab(label='Distribuzione Termini', children=[
                                    	html.Div([html.H2("Numero di 0 e 1 per ogni termine")], style={"textAlign": "center"}),
											html.Div([
											html.Span("Seleziona il termine", style={"width": 150, "padding": 10, "text-align": "right"}),
											html.Div(dcc.Dropdown(id="xaxis",
											  	options=[{'label': "great", 'value': "great"},
														{'label': "good", 'value': "good"},
														{'label': "comfortable", 'value': "comfortable"},
														{'label': "bad", 'value': "bad"},
														{'label': "old", 'value': "old"},
											   			], 
												value='great', 
											), 
											style={"width": 250, "margin": 0}
										)], 
										className="row"),
												
										html.Div([dcc.Graph(id="output")])
                                    ]),
                                    dcc.Tab(label='Distribuzione Metadati', children=[
                                            dcc.Graph(
                                                id='example-graph-2',
                                                figure={
                                                    'data': [
                                                        {'x': ['Rooms: 1', 'Value: 1', 'Location: 1', 'Cleanliness: 1', 'Service: 1'],
                                                            'y': [RoomsDict[1], ValueDict[1], LocationDict[1], CleanlinessDict[1], ServiceDict[1]],
                                                            'type': 'bar', 'name': '1'},
                                                        {'x': ['Rooms: 2', 'Value: 2', 'Location: 2', 'Cleanliness: 2', 'Service: 2'],
                                                            'y': [RoomsDict[2], ValueDict[2], LocationDict[2], CleanlinessDict[2], ServiceDict[2]],
                                                            'type': 'bar', 'name': '2'},
                                                        {'x': ['Rooms: 3', 'Value: 3', 'Location: 3', 'Cleanliness: 3', 'Service: 3'],
                                                            'y': [RoomsDict[3], ValueDict[3], LocationDict[3], CleanlinessDict[3], ServiceDict[3]],
                                                            'type': 'bar', 'name': '3'},
                                                        {'x': ['Rooms: 4', 'Value: 4', 'Location: 4', 'Cleanliness: 4', 'Service: 4'],
                                                            'y': [RoomsDict[4], ValueDict[4], LocationDict[4], CleanlinessDict[4], ServiceDict[4]],
                                                            'type': 'bar', 'name': '4'},
                                                        {'x': ['Rooms: 5', 'Value: 5', 'Location: 5', 'Cleanliness: 5', 'Service: 5'],
                                                            'y': [RoomsDict[5], ValueDict[5], LocationDict[5], CleanlinessDict[5], ServiceDict[5]],
                                                            'type': 'bar', 'name': '5'},
                                                    ]
                                                }
                                            )
                                    ]),
                            ]),
                ]),

        #Fourth tab
		dcc.Tab(label='Feature selection e vettorizzazione',
        		style=tab_style,
                selected_style=tab_selected_style,
                children=[
                    html.Div([
                        html.Div([
                        		html.H1('Feature Selection'),
                        		html.P('1. Tokenization'),
                        		html.P('2. Portare tutti i testi in lower-case'),
                        		html.P('3. Rimozione di Stopwords e della Punteggiatura'),
                        		html.P('4. Selezione degli aggettivi più frequenti: 3 positivi e 3 negativi'),
                        		html.P('5. Costruzione del vettore')
                            	], style={'color': 'black', 'marginTop': '5%', 'width':'49%', 'display':'inline-block', 'fontSize':20}),

                        html.Div([
								html.H1('Rappresentazione Vettoriale'),
								generate_table(df)
						], style={'color': 'black',  'marginTop': '5%', 'width':'40%', 'display':'inline-block', 'vertical-align': 'top'})
                        ])


                    ]),
        	#Third tab
    		dcc.Tab(label='Rete Bayesiana',
            		style=tab_style,
                    selected_style=tab_selected_style,
                    children=[
                        html.Div([
                            html.Div([
                                    html.H1('Rete di bayes knowledge based'),
                            		html.Img(src='data:image/png;base64,{}'.format(encoded_image6.decode())),
                                	], style={'color': 'black', 'marginLeft': '4%', 'marginTop': '5%', 'width':'40%', 'display':'inline-block', 'vertical-align': 'top'}),

                            html.Div([
                                    html.H1('Rete di bayes appresa'),
    								html.Img(src='data:image/png;base64,{}'.format(encoded_image7.decode())),
    						], style={'color': 'black',  'marginTop': '5%', 'width':'40%', 'display':'inline-block', 'vertical-align': 'top'})
                            ])


                        ]),
		#prova
		dcc.Tab(label='Prova',
			style=tab_style,
			selected_style=tab_selected_style,
			children=[
				html.Div([
					html.Label('Query'),
					dcc.Dropdown(
						options=[
							{'label': 'Overall', 'value': 'OVR'}
						],
						value='MTL',
						placeholder="Select the query variable",
					),

					html.Label('Evidence variables'),
					dcc.Dropdown(
						options=[
							{'label': 'good', 'value': 'good'},
							{'label': 'great', 'value': 'great'},
							{'label': 'comfortable', 'value': 'comfortable'},
							{'label': 'small', 'value': 'small'},
							{'label': 'bad', 'value': 'bad'},
							{'label': 'old', 'value': 'old'},
							{'label': 'Value', 'value': 'Value'},
							{'label': 'Rooms', 'value': 'Rooms'},
							{'label': 'Location', 'value': 'Location'},
							{'label': 'Service', 'value': 'Service'},
							{'label': 'Cleanliness', 'value': 'Cleanliness'}
						],
						placeholder="Select at least an evidence variable",
						searchable = True,
						value='MTL',
						multi=True
					),
				], style={'columnCount': 1})

			])

		])
	])
	
@app.callback(
dash.dependencies.Output("output", "figure"),
[dash.dependencies.Input("xaxis", "value")])

def update_output(selected):
	c = Counter(df[selected])
	trace = [go.Bar(x=[selected+' con 0', selected+' con 1'], y=[c[0], c[1]], xaxis='x3', yaxis='y3', showlegend=False,
	marker={"color": "#5DB4F2", "opacity": 0.9})]
	return {"data": trace}
	
if __name__ == '__main__':
    app.run_server(debug=True)
