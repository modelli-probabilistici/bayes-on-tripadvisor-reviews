#!/usr/bin/env python
# coding=utf-8
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd
import os
import plotly.plotly as py
import plotly.presentation_objs as pres
import base64
import pandas as pd
from collections import Counter
from dash.dependencies import Input, Output, State
from libpgm.nodedata import NodeData
from libpgm.graphskeleton import GraphSkeleton
from libpgm.discretebayesiannetwork import DiscreteBayesianNetwork
from libpgm.pgmlearner import PGMLearner
from libpgm.tablecpdfactorization import TableCPDFactorization

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

server = app.server

tabs_styles = {
    'height': '44px'
}
tab_style = {
    'borderBottom': '1px solid #d6d6d6',
    'padding': '6px',
    'fontWeight': 'bold'
}

tab_selected_style = {
    'borderTop': '1px solid #d6d6d6',
    'borderBottom': '1px solid #d6d6d6',
    'backgroundColor': '#119DFF',
    'color': 'white',
    'padding': '6px'
}

colors = {
    'background': '#111111',
    'text': '#7FDBFF'
}

#import dataset
df1 = pd.read_csv('../test-valid.csv')
df = pd.read_csv('../vettori-valid.csv')
acc = pd.read_csv('../accuracy.csv')
OverallDictTest = df1['Overall'].value_counts().to_dict()
OverallDict = df['Overall'].value_counts().to_dict()
RoomsDict = df['Rooms'].value_counts().to_dict()
ValueDict = df['Value'].value_counts().to_dict()
LocationDict = df['Location'].value_counts().to_dict()
CleanlinessDict = df['Cleanliness'].value_counts().to_dict()
ServiceDict = df['Service'].value_counts().to_dict()

#function to generate a html table given a dataframe pandas
def generate_table(dataframe, max_rows=10):
    return html.Table(
        # Header
        [html.Tr([html.Th(col) for col in dataframe.columns])] +

        # Body
        [html.Tr([
            html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
        ]) for i in range(min(len(dataframe), max_rows))]
    )

def format_data(df):
    result = []
    for row in df.itertuples():
        #print(row.Pclass)
        result.append(dict(comfortable = row.comfortable, great = row.great, good = row.good, small = row.small, bad = row.bad, old = row.old, Cleanliness= row.Cleanliness,
            Location=row.Location, Service=row.Service, Rooms=row.Rooms, Value=row.Value, Overall=row.Overall ))
    return result

#import images
image_filename1 = '../Img/sample-review.png'
encoded_image1 = base64.b64encode(open(image_filename1, 'rb').read())

image_filename2 = '../Img/bayes-net.png'
encoded_image2 = base64.b64encode(open(image_filename2, 'rb').read())

image_filename3 = '../Img/bayes-net2.png'
encoded_image3 = base64.b64encode(open(image_filename3, 'rb').read())

image_filename4 = '../Img/slide1.png'
encoded_image4 = base64.b64encode(open(image_filename4, 'rb').read())

image_filename5 = '../Img/slide2.png'
encoded_image5 = base64.b64encode(open(image_filename5, 'rb').read())

image_filename8 = '../Img/slide3.png'
encoded_image8= base64.b64encode(open(image_filename8, 'rb').read())

image_filename9 = '../Img/slide4.png'
encoded_image9 = base64.b64encode(open(image_filename9, 'rb').read())

image_filename10 = '../Img/slide5.png'
encoded_image10 = base64.b64encode(open(image_filename10, 'rb').read())

image_filename6 = '../Img/bayes-net.png'
encoded_image6 = base64.b64encode(open(image_filename6, 'rb').read())

image_filename7 = '../Img/bayes-net2.png'
encoded_image7 = base64.b64encode(open(image_filename7, 'rb').read())


#load tabs
app.layout = html.Div([
    dcc.Tabs(id='tabs', style=tabs_styles, children=[
        #Presentazione tab
        dcc.Tab(label='Presentazione',
                style=tab_style,
                selected_style=tab_selected_style,
                children=[
                    html.Div([
                        html.Div([
                            html.Img(src='data:image/png;base64,{}'.format(encoded_image4.decode()),
                                    style={
                                        'height' : '80%',
                                        'width' : '100%',
                                        'float' : 'left',
                                        'position' : 'relative',
                                        'padding-top' : 0,
                                        'padding-right' : 0
                            }),
                        ]),
                    ])
		]),
		#Dati tab
		dcc.Tab(label='Dati',
                style=tab_style,
                selected_style=tab_selected_style,
                children=[
                        dcc.Tabs(id="subtabs", children=[
                                    dcc.Tab(label='Formato iniziale dei dati', children=[
                                        html.Div([
                                            html.H1('Formato iniziale dei dati', style={'textAlign': 'center', 'color': 'black'}),
                                            html.Div([
                        							html.Img(src='data:image/png;base64,{}'.format(encoded_image5.decode())),
                        					], style={'color': 'black', 'marginLeft': '0%', 'marginTop': '0%', 'width':'40%', 'display':'inline-block', 'vertical-align': 'top'}),
                                            html.Div([
                                                html.Img(src='data:image/png;base64,{}'.format(encoded_image1.decode()), style={'width': '104%'}),
                                            ],  style={'color': 'black', 'marginLeft': '8%', 'marginTop': '0%', 'width':'48%', 'display':'inline-block', 'vertical-align': 'top'})
                                        ], style={'marginTop': '3%'})
                    				]),
                                    dcc.Tab(label='Distribuzione Overall', children=[
                                        html.Div([
                                            #Istogramma distribuzione Overall
                                            html.H1('Distribuzione Overall', style={'textAlign': 'center', 'color': 'black'}),
                                            dcc.Graph(
                                                id='distOverall',
                                                figure={
                                                    'data': [
                                                        {'x': [1], 'y': [OverallDict[1]],
                                                            'type': 'bar', 'name': 'Overall: 1'},
                                                        {'x': [2], 'y': [OverallDict[2]],
                                                            'type': 'bar', 'name': 'Overall: 2'},
                                                        {'x': [3], 'y': [OverallDict[3]],
                                                            'type': 'bar', 'name': 'Overall: 3'},
                                                        {'x': [4], 'y': [OverallDict[4]],
                                                            'type': 'bar', 'name': 'Overall: 4'},
                                                        {'x': [5], 'y': [OverallDict[5]],
                                                            'type': 'bar', 'name': 'Overall: 5'},
                                                    ]
                                                }
                                            )
                                        ], style = {'marginTop': '5%'})
                                    ]),
                                    dcc.Tab(label='Distribuzione Termini', children=[
                                        #Istogramma distribuzione Termini
                                    	html.Div([html.H2("Numero di 0 e 1 per ogni termine")], style={"textAlign": "center", 'marginTop': '5%'}),
										html.Div([
											html.Span("Seleziona il termine", style={"width": 150, "padding": 10, "text-align": "right"}),
											html.Div(
                                                dcc.Dropdown(id="xaxis",
    											  	options=[{'label': "great", 'value': "great"},
    														{'label': "good", 'value': "good"},
    														{'label': "comfortable", 'value': "comfortable"},
    														{'label': "bad", 'value': "bad"},
    														{'label': "old", 'value': "old"},
                                                            {'label': "small", 'value': "small"}
    											   			],
    												value='great',),
    									            style={"width": 250, "margin": 0, 'marginTop': '5%'}
										    )],
										className="row"),

										html.Div([dcc.Graph(id="output")])
                                    ]),
                                    dcc.Tab(label='Distribuzione Metadati', children=[
                                            html.Div([
                                                html.H1('Distribuzione Metadati', style={'textAlign': 'center', 'color': 'black', 'marginTop': '5%'}),
                                                #Istogramma distribuzione Metadati
                                                dcc.Graph(
                                                    id='distMetadati',
                                                    figure={
                                                        'data': [
                                                            {'x': ['Rooms: 1', 'Value: 1', 'Location: 1', 'Cleanliness: 1', 'Service: 1'],
                                                                'y': [RoomsDict[1], ValueDict[1], LocationDict[1], CleanlinessDict[1], ServiceDict[1]],
                                                                'type': 'bar', 'name': '1'},
                                                            {'x': ['Rooms: 2', 'Value: 2', 'Location: 2', 'Cleanliness: 2', 'Service: 2'],
                                                                'y': [RoomsDict[2], ValueDict[2], LocationDict[2], CleanlinessDict[2], ServiceDict[2]],
                                                                'type': 'bar', 'name': '2'},
                                                            {'x': ['Rooms: 3', 'Value: 3', 'Location: 3', 'Cleanliness: 3', 'Service: 3'],
                                                                'y': [RoomsDict[3], ValueDict[3], LocationDict[3], CleanlinessDict[3], ServiceDict[3]],
                                                                'type': 'bar', 'name': '3'},
                                                            {'x': ['Rooms: 4', 'Value: 4', 'Location: 4', 'Cleanliness: 4', 'Service: 4'],
                                                                'y': [RoomsDict[4], ValueDict[4], LocationDict[4], CleanlinessDict[4], ServiceDict[4]],
                                                                'type': 'bar', 'name': '4'},
                                                            {'x': ['Rooms: 5', 'Value: 5', 'Location: 5', 'Cleanliness: 5', 'Service: 5'],
                                                                'y': [RoomsDict[5], ValueDict[5], LocationDict[5], CleanlinessDict[5], ServiceDict[5]],
                                                                'type': 'bar', 'name': '5'},
                                                        ]
                                                    }
                                                )
                                            ], style = {'marginTop': '5%'}),
                                    ]),
                            ]),
                ]),
        #Feature selection tab
		dcc.Tab(label='Feature selection',
        		style=tab_style,
                selected_style=tab_selected_style,
                children=[
                    html.Div([
                        html.H1('Rappresentazione Vettoriale', style={'textAlign': 'center', 'color': 'black'}),
                        html.Div([
                            html.Img(src='data:image/png;base64,{}'.format(encoded_image8.decode())),
                        ],  style={'width':'49%', 'display':'inline-block', 'marginTop':'0%', 'color':'black'}),
                        html.Div([
								generate_table(df)
						], style={'color': 'black', 'marginLeft': '-5%', 'marginTop': '0%', 'width':'30%', 'display':'inline-block', 'vertical-align': 'top'})
                        ], style={'marginTop': '3%'})


        ]),
    	#Rete Bayesiana tab
		dcc.Tab(label='Rete Bayesiana',
        		style=tab_style,
                selected_style=tab_selected_style,
                children=[
                    html.Div([
                        html.Div([
                                html.H1('Rete di bayes knowledge based'),
                        		html.Img(src='data:image/png;base64,{}'.format(encoded_image6.decode())),
                            	], style={'color': 'black', 'marginLeft': '4%', 'marginTop': '5%', 'width':'40%', 'display':'inline-block', 'vertical-align': 'top'}),

                        html.Div([
                                html.H1('Rete di bayes appresa', style={'textAlign': 'center', 'color': 'black'}),
								html.Img(src='data:image/png;base64,{}'.format(encoded_image7.decode())),
						], style={'color': 'black',  'marginTop': '5%', 'width':'40%', 'display':'inline-block', 'vertical-align': 'top'})
                    ])
        ]),
        #Test modello tab
		dcc.Tab(label='Test del modello',
        		style=tab_style,
                selected_style=tab_selected_style,
                children=[
                        dcc.Tabs(id="subtabs2", children=[
                                    dcc.Tab(label='Distribuzione Overall nel Test', children=[
                                        html.Div([
                                            html.H1('Distribuzione Overall', style={'textAlign': 'center', 'color': 'black'}),
                                            dcc.Graph(
                                                id='distOverallTest',
                                                figure={
                                                    'data': [
                                                        {'x': [1], 'y': [OverallDictTest[1]],
                                                            'type': 'bar', 'name': 'Overall: 1'},
                                                        {'x': [2], 'y': [OverallDictTest[2]],
                                                            'type': 'bar', 'name': 'Overall: 2'},
                                                        {'x': [3], 'y': [OverallDictTest[3]],
                                                            'type': 'bar', 'name': 'Overall: 3'},
                                                        {'x': [4], 'y': [OverallDictTest[4]],
                                                            'type': 'bar', 'name': 'Overall: 4'},
                                                        {'x': [5], 'y': [OverallDictTest[5]],
                                                            'type': 'bar', 'name': 'Overall: 5'},
                                                    ]
                                                }
                                            )
                                        ], style = {'marginTop': '5%'})
                                    ]),
                                    dcc.Tab(label='Risultati', children=[
                                        html.Div([
                                            html.H1('Analisi dei risultati', style = {'textAlign': 'center'}),
                                            html.Div([
                                                html.Img(src='data:image/png;base64,{}'.format(encoded_image9.decode())),
                                            ], style={'color': 'black', 'marginLeft': '0%', 'marginTop': '0%', 'width':'42%', 'display':'inline-block', 'vertical-align': 'top'}),
                                            html.Div([
                                                generate_table(acc,6)
                                            ], style={'color': 'black', 'marginLeft': '10%', 'marginTop': '5%', 'width':'42%', 'display':'inline-block', 'vertical-align': 'top'})
                                        ], style = {'marginTop': '5%'}),
                                    ]),
                                ]),
        ]),
		#Inferenza tab
		dcc.Tab(label='Inferenza',
			style=tab_style,
			selected_style=tab_selected_style,
			children=[
                html.Div([
                html.H1('Query', style = {'textAlign': 'center'}),

                html.Div([
                    #dropdown where to select query variable
                    dcc.Dropdown(
                        id='Query',
                        options=[
                            {'label': 'good', 'value': 'good'},
                            {'label': 'great', 'value': 'great'},
                            {'label': 'comfortable', 'value': 'comfortable'},
                            {'label': 'small', 'value': 'small'},
                            {'label': 'bad', 'value': 'bad'},
                            {'label': 'old', 'value': 'old'},
                            {'label': 'Value', 'value': 'Value'},
                            {'label': 'Rooms', 'value': 'Rooms'},
                            {'label': 'Location', 'value': 'Location'},
                            {'label': 'Service', 'value': 'Service'},
                            {'label': 'Cleanliness', 'value': 'Cleanliness'},
                            {'label': 'Overall', 'value': 'Overall'}
                        ],
                        value = "Overall",
                        placeholder="Select the query variable"
                    )

                ], style = {'columnCount': 2, 'margin-top': '1%'}),

                html.H3('Evidence values'),
                #items to select evidence variables' values
                html.Div([
                    #each RadioItems is used to set a single evidence variable value
                    dcc.RadioItems(
                        id = 'good value',
                        options = [
                            {'label':'good = 0', 'value': '0'},
                            {'label':'good = 1', 'value': '1'}
                            ],
                        style={'display': 'block'}
                    ),
                    dcc.RadioItems(
                        id = 'great value',
                        options = [
                            {'label':'great = 0', 'value': '0'},
                            {'label':'great = 1', 'value': '1'}
                            ],
                        style={'display': 'block'}
                    ),
                    dcc.RadioItems(
                        id = 'comfortable value',
                        options = [
                            {'label':'comfortable = 0', 'value': '0'},
                            {'label':'comfortable = 1', 'value': '1'}
                            ],
                        style={'display': 'block'}
                    ),
                    dcc.RadioItems(
                        id = 'bad value',
                        options = [
                            {'label':'bad = 0', 'value': '0'},
                            {'label':'bad = 1', 'value': '1'}
                            ],
                        style={'display': 'block'}
                    ),
                    dcc.RadioItems(
                        id = 'old value',
                        options = [
                            {'label':'old = 0', 'value': '0'},
                            {'label':'old = 1', 'value': '1'}
                            ],
                        style={'display': 'block'}
                    ),

                    dcc.RadioItems(
                        id = 'small value',
                        options = [
                            {'label':'small = 0', 'value': '0'},
                            {'label':'small = 1', 'value': '1'}
                            ],
                        style={'display': 'block'}
                    ),

                    dcc.RadioItems(
                        id = 'Rooms value',
                        options = [
                            {'label':'Rooms = 1', 'value': '1'},
                            {'label':'Rooms = 2', 'value': '2'},
                            {'label':'Rooms = 3', 'value': '3'},
                            {'label':'Rooms = 4', 'value': '4'},
                            {'label':'Rooms = 5', 'value': '5'}
                            ],
                        style={'display': 'block'}
                    ),

                    dcc.RadioItems(
                        id = 'Service value',
                        options = [
                            {'label':'Service = 1', 'value': '1'},
                            {'label':'Service = 2', 'value': '2'},
                            {'label':'Service = 3', 'value': '3'},
                            {'label':'Service = 4', 'value': '4'},
                            {'label':'Service = 5', 'value': '5'}
                            ],
                        style={'display': 'block'}
                    ),

                    dcc.RadioItems(
                        id = 'Cleanliness value',
                        options = [
                            {'label':'Cleanliness = 1', 'value': '1'},
                            {'label':'Cleanliness = 2', 'value': '2'},
                            {'label':'Cleanliness = 3', 'value': '3'},
                            {'label':'Cleanliness = 4', 'value': '4'},
                            {'label':'Cleanliness = 5', 'value': '5'}
                            ],
                        style={'display': 'block'}
                    ),

                    dcc.RadioItems(
                        id = 'Location value',
                        options = [
                            {'label':'Location = 1', 'value': '1'},
                            {'label':'Location = 2', 'value': '2'},
                            {'label':'Location = 3', 'value': '3'},
                            {'label':'Location = 4', 'value': '4'},
                            {'label':'Location = 5', 'value': '5'}
                            ],
                        style={'display': 'block'}
                    ),

                    dcc.RadioItems(
                        id = 'Overall value',
                        options = [
                            {'label':'Overall = 1', 'value': '1'},
                            {'label':'Overall = 2', 'value': '2'},
                            {'label':'Overall = 3', 'value': '3'},
                            {'label':'Overall = 4', 'value': '4'},
                            {'label':'Overall = 5', 'value': '5'}
                            ],
                        style={'display': 'block'}
                    ),

                    dcc.RadioItems(
                        id = 'Value value',
                        options = [
                            {'label':'Value = 1', 'value': '1'},
                            {'label':'Value = 2', 'value': '2'},
                            {'label':'Value = 3', 'value': '3'},
                            {'label':'Value = 4', 'value': '4'},
                            {'label':'Value = 5', 'value': '5'}
                            ],
                        style={'display': 'block'}
                    ),

            ],style={'columnCount': 4}),

            #graph where is shown the probability distribution of the query variable given evidences
            dcc.Graph(
                id='queryresult'
            )
            ], style = {'marginTop': '3%'})


			]),
        #Conclusioni tab
        dcc.Tab(label='Conclusioni',
    			style=tab_style,
    			selected_style=tab_selected_style,
    			children=[
                    html.Div([
                        html.Div([
                            html.Img(src='data:image/png;base64,{}'.format(encoded_image10.decode()),
                                    style={
                                        'height' : '70%',
                                        'width' : '100%',
                                        'float' : 'left',
                                        'position' : 'relative',
                                        'padding-top' : 0,
                                        'padding-right' : 0
                            }),
                        ]),
                    ])
                ])
            ])
	])

#callback to generate a bar plot given the selection of a scrolldown bar
@app.callback(
dash.dependencies.Output("output", "figure"),
[dash.dependencies.Input("xaxis", "value")])
def update_output(selected):
	c = Counter(df[selected])
	trace = [go.Bar(x=[selected+' con 0', selected+' con 1'], y=[c[0], c[1]], xaxis='x3', yaxis='y3', showlegend=False,
	marker={"color": "#5DB4F2", "opacity": 0.9})]
	return {"data": trace}

#function that dynamically update the graph for every new evidence variable value or for a new query variable
@app.callback(
    Output('queryresult', 'figure'),
    [Input('Query', 'value'),
    Input('Value value', 'value'),
    Input('Rooms value', 'value'),
    Input('Location value', 'value'),
    Input('Overall value', 'value'),
    Input('Service value', 'value'),
    Input('Cleanliness value', 'value'),
    Input('good value', 'value'),
    Input('great value', 'value'),
    Input('comfortable value', 'value'),
    Input('small value', 'value'),
    Input('old value', 'value'),
    Input('bad value', 'value')]
    )
def update_graph(QueryVariable, ValueValue, RoomsValue, LocationValue, OverallValue, ServiceValue, CleanlinessValue,
goodValue, greatValue, comfortableValue, smallValue, oldValue, badValue):
    #load all preprocessed training data
    df = pd.read_csv('../vettori-valid.csv', sep=',')
    #format data to let them correctly processed by libpgm functions
    node_data = format_data(df)
    skel = GraphSkeleton()
    #load structure of our net
    skel.load("../json_skel.txt")
    #setting the topologic order
    skel.toporder()
    #learner which will estimate parameters
    learner = PGMLearner()
    #estismting parameters for our own model
    model = learner.discrete_mle_estimateparams(skel, node_data)
    #creating a dict containing evidences variables values if set
    evidences = dict()
    if ValueValue is not None and QueryVariable != 'Value':
        evidences.update({'Value' : int(ValueValue)})
    if RoomsValue is not None and QueryVariable != 'Rooms':
        evidences.update({'Rooms' : int(RoomsValue)})
    if LocationValue is not None and QueryVariable != 'Location':
        evidences.update({'Location' : int(LocationValue)})
    if OverallValue is not None and QueryVariable != 'Overall':
        evidences.update({'Overall' : int(OverallValue)})
    if ServiceValue is not None and QueryVariable != 'Service':
        evidences.update({'Service' : int(ServiceValue)})
    if CleanlinessValue is not None and QueryVariable != 'Cleanliness':
        evidences.update({'Cleanliness' : int(CleanlinessValue)})
    if goodValue is not None and QueryVariable != 'good':
        evidences.update({'good' : int(goodValue)})
    if greatValue is not None and QueryVariable != 'great':
        evidences.update({'great' : int(greatValue)})
    if comfortableValue is not None and QueryVariable != 'comfortable':
        evidences.update({'comfortable' : int(comfortableValue)})
    if smallValue is not None and QueryVariable != 'small':
        evidences.update({'small' : int(smallValue)})
    if oldValue is not None and QueryVariable != 'old':
        evidences.update({'old' : int(oldValue)})
    if badValue is not None and QueryVariable != 'bad':
        evidences.update({'bad' : int(badValue)})
    #creating a dict containing query value
    query = {str(QueryVariable) : 1}
    #getting all cpt from our model
    a = TableCPDFactorization(model)
    #run inference on the net given queries and evidences
    result = a.condprobve(query, evidences)
    #defining graph plotting for 5values variables
    if QueryVariable in ['Rooms', 'Cleanliness', 'Value', 'Service', 'Overall', 'Location']:
        trace = go.Bar(
            x = [1,2,3,4,5],
            y = result.vals
        )
        return {
            'data': [trace],
            'layout': go.Layout(
                title='Query Graph',
                xaxis=go.layout.XAxis(range=[0,6]),
                hovermode='closest'
            )
        }
    else:
        #defining graph plotting for binary variables 
        trace = go.Bar(
            x = [0, 1],
            y = result.vals
        )
        return {
            'data': [trace],
            'layout': go.Layout(
                title='Query Graph',
                xaxis=go.layout.XAxis(range=[-1, 2]),
                hovermode='closest'
            )
        }

#function that dynamically hides radioitems for query variable
@app.callback([
    Output('Value value', 'style'),
    Output('Cleanliness value', 'style'),
    Output('Overall value', 'style'),
    Output('Service value', 'style'),
    Output('Location value', 'style'),
    Output('Rooms value', 'style'),
    Output('good value', 'style'),
    Output('great value', 'style'),
    Output('comfortable value', 'style'),
    Output('bad value', 'style'),
    Output('old value', 'style'),
    Output('small value', 'style'),
    ], [Input('Query', 'value')])
def showevidences(value):
    l = []
    #searching the query value
    for item in [value]:
        lst = []
        if item == 'Value':
            lst.append({'display':'none'})
        else:
            lst.append({'display':'block'})
        if item == 'Cleanliness':
            lst.append({'display':'none'})
        else:
            lst.append({'display':'block'})
        if item == 'Overall':
            lst.append({'display':'none'})
        else:
            lst.append({'display':'block'})
        if item == 'Service':
            lst.append({'display':'none'})
        else:
            lst.append({'display':'block'})
        if item == 'Location':
            lst.append({'display':'none'})
        else:
            lst.append({'display':'block'})
        if item == 'Rooms':
            lst.append({'display':'none'})
        else:
            lst.append({'display':'block'})
        if item == 'good':
            lst.append({'display':'none'})
        else:
            lst.append({'display':'block'})
        if item == 'great':
            lst.append({'display':'none'})
        else:
            lst.append({'display':'block'})
        if item == 'comfortable':
            lst.append({'display':'none'})
        else:
            lst.append({'display':'block'})
        if item == 'bad':
            lst.append({'display':'none'})
        else:
            lst.append({'display':'block'})
        if item == 'old':
            lst.append({'display':'none'})
        else:
            lst.append({'display':'block'})
        if item == 'small':
            lst.append({'display':'none'})
        else:
            lst.append({'display':'block'})
        l.append(lst)
    final = [{'display':'block'},{'display':'block'},{'display':'block'},{'display':'block'},{'display':'block'},
            {'display':'block'},{'display':'block'},{'display':'block'},{'display':'block'},{'display':'block'},
            {'display':'block'},{'display':'block'}]
    #setting it's style to none so i can hide it
    for seq in l:
        for i in range(0, len(seq)):
            if(seq[i] == {'display':'none'}):
                final[i] = {'display':'none'}
    return final
#function to hide the graph if no variable is selected as query
@app.callback(
    Output('queryresult', 'style'),
    [Input('Query', 'value')])
def showqueriestaxtarea(value):
        if value:
           return {'display':'block'}
        else:
            return {'display':'none'}

if __name__ == '__main__':
    app.run_server(debug=True)
