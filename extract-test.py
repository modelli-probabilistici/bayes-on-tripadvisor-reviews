import requests
from itertools import cycle
from bs4 import BeautifulSoup
from lxml.html import fromstring
import urllib.request
import random
import time
import pandas as pd
import os
import csv

#this file works the same as extract-extended, but will works on test data

def separateByClass(dataset):
	separated = {}
	for i in range(len(dataset)):
		vector = dataset[i]
		if (vector[-1] not in separated):
			separated[vector[-1]] = []
		separated[vector[-1]].append(vector)
	return separated

path = 'Testing/'
files = os.listdir(path)
featurelst = []
print(len(files))
with open('test.csv', 'w') as vettori:
	header = ['ReviewID', 'great', 'good', 'comfortable', 'small', 'bad', 'old', 'Value', 'Rooms', 'Location', 'Cleanliness','Service', 'Overall']

	writer = csv.writer(vettori)
	writer.writerow(header)

	#Inizializziamo le variabili a 0
	count=1
	great=0
	good=0
	comfortable=0
	bad=0
	small=0
	old=0
	overall=0
	value=0
	check=0
	location=0
	cleanlines=0
	service=0
	business=0

	for f in files:
		if os.path.isfile(path + f):
			print(str(f))
			datContent = [i.strip().split() for i in open("Testing/" +f,"r").readlines()]
			datContent = datContent[4: -1]
			for item in datContent:

				'''terms'''
				if(item and item[0].startswith('<Content>')):
					great=0
					good=0
					comfortable=0
					small=0
					bad=0
					old=0
					for el in item:
						if(el.startswith('great') or el.startswith('great,') or el.startswith('great.')):
							great=1
						elif(el.startswith('good') or el.startswith('good,') or el.startswith('good.')):
							good=1
						elif(el.startswith('comfortable') or el.startswith('comfortable,') or el.startswith('comfortable.')):
							comfortable=1
						elif(el.startswith('small') or el.startswith('small,') or el.startswith('small.')):
							small=1
						elif(el.startswith('bad') or el.startswith('bad,') or el.startswith('bad.')):
							bad=1
						elif(el.startswith('old') or el.startswith('old,') or el.startswith('old.')):
							old=1

				'''meta-dati'''
				for element in item:
					if element.startswith("<Overall>"):
						overall = element.replace('<Overall>','')
					if element.startswith("<Value>"):
						value = element.replace('<Value>','')
					if element.startswith("<Rooms>"):
						rooms = element.replace('<Rooms>','')
					if element.startswith("<Location>"):
						location = element.replace('<Location>','')
					if element.startswith("<Cleanliness>"):
						cleanlines = element.replace('<Cleanliness>','')
					if element.startswith('<Service>'):
						service = element.replace('<Service>', '')



				if(not item):
					if(count>1):

						writer.writerow([count-1, great, good, comfortable, small, bad, old, value, rooms, location, cleanlines, service, overall])
					count=count+1
