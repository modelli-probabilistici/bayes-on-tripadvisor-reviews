import nltk
from nltk.tag import *
import pandas as pd
from nltk.tokenize import word_tokenize
from collections import Counter
import itertools
from nltk.corpus import stopwords
import string
nltk.download('averaged_perceptron_tagger')

#define the header
h = ['ID', 'Text']
df = pd.read_csv('Text.csv', skiprows=1, header=None, names=h, sep='\t')
#Convert all reviews in lower case
text = df['Text']
df['Text'] = list(map(lambda x: x.lower(), text))

#define a list of contractions as a dict
contractions = { 
"ain't": "am not",
"aren't": "are not",
"can't": "cannot",
"can't've": "cannot have",
"'cause": "because",
"could've": "could have",
"couldn't": "could not",
"couldn't've": "could not have",
"didn't": "did not",
"doesn't": "does not",
"don't": "do not",
"hadn't": "had not",
"hadn't've": "had not have",
"hasn't": "has not",
"haven't": "have not",
"he'd": "he would",
"he'd've": "he would have",
"he'll": "he will",
"he's": "he is",
"how'd": "how did",
"how'll": "how will",
"how's": "how is",
"i'd": "i would",
"i'll": "i will",
"i'm": "i am",
"i've": "i have",
"isn't": "is not",
"it'd": "it would",
"it'll": "it will",
"it's": "it is",
"let's": "let us",
"ma'am": "madam",
"mayn't": "may not",
"might've": "might have",
"mightn't": "might not",
"must've": "must have",
"mustn't": "must not",
"needn't": "need not",
"oughtn't": "ought not",
"shan't": "shall not",
"sha'n't": "shall not",
"she'd": "she would",
"she'll": "she will",
"she's": "she is",
"should've": "should have",
"shouldn't": "should not",
"that'd": "that would",
"that's": "that is",
"there'd": "there had",
"there's": "there is",
"they'd": "they would",
"they'll": "they will",
"they're": "they are",
"they've": "they have",
"wasn't": "was not",
"we'd": "we would",
"we'll": "we will",
"we're": "we are",
"we've": "we have",
"weren't": "were not",
"what'll": "what will",
"what're": "what are",
"what's": "what is",
"what've": "what have",
"where'd": "where did",
"where's": "where is",
"who'll": "who will",
"who's": "who is",
"won't": "will not",
"wouldn't": "would not",
"you'd": "you would",
"you'll": "you will",
"you're": "you are"
}

#de-contract words
def clean(text):
  text = text.split()
  new_text = []
  for word in text:
    if word in contractions:
      new_text.append(contractions[word])
    else:
      new_text.append(word)
  text = " ".join(new_text)
  return text
      
clean_text = []
for text in df.Text:
   clean_text.append(clean(text))
df['clean_text'] = clean_text
df.head()

#Let's tokenize the reviews using word_tokenize from nltk
df['tokenized_clean'] = df.apply(lambda row: word_tokenize(row['clean_text']), axis=1)
clean_tokenized = df['tokenized_clean']

#remove stopwords
stop =stopwords.words('english')
text_tokenized_stop = clean_tokenized.apply(lambda x: [item for item in x if item not in stop])
sentences = (list(itertools.chain(text_tokenized_stop)))

#remove puntaction
punctuation = string.punctuation
text_tokenized_stop_punct = text_tokenized_stop.apply(lambda x: [item for item in x if item not in punctuation])
sentences = (list(itertools.chain(text_tokenized_stop_punct)))
flat_list = [item for sublist in sentences for item in sublist]

#take most common adjectives
tagged_sent = pos_tag(flat_list)
adj = [word for word,pos in tagged_sent if pos == 'JJ']
flat_list = [item for item in adj]
c = Counter(flat_list)
print(c.most_common(30))


