import pandas as pd
import csv

#function to remove row with not valide meta-data value
def remove():
	with open('test.csv', 'r') as infile, open('test-valid.csv', 'w') as outfile:
		reader = csv.reader(infile)
		writer = csv.writer(outfile)
		for row in reader:
			if(row[7]=='-1' or row[8] =='-1' or row[9]=='-1' or row[10]=='-1' or row[11]=='-1'):
				print(row)
			else:
				writer.writerow(row)
#function to remove the id column
def reID():
	df = pd.read_csv('test-valid.csv')
	new_df = df.drop(df.columns[0], axis=1)
	new_df.to_csv('test-valid.csv', index=False)
	
remove()
reID()
